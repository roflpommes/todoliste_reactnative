import 
  React, 
  { useEffect, 
    useState
  } from 'react';

import moment from 'moment'

import {
  View,
  TouchableOpacity,
  Modal,
  Image, 
  Text, 
  SafeAreaView,
  StatusBar,
  TextInput, 
  Button, 
  StyleSheet,
  FlatList,
  Alert
  } from 'react-native';

import {openDatabase} from 'react-native-sqlite-storage';
//import DatePicker from 'react-native-datepicker'
import DateTimePicker from '@react-native-community/datetimepicker';

const db = openDatabase({
  name: "TodoList",
});

const App = () => {
  const [t_id, setTodoId] = useState("");
  const [t_Name, setTodoName] = useState("");
  const [t_Note, setTodoNote] = useState("");
  //const [t_Date, setTodoDate] = useState(moment().format("YYYY-MM-DD"));
  const [t_Date, setTodoDate] = useState(new Date());
  const [t_Time, setTodoTime] = useState(new Date());
  const [t_Done, setTodoDone] = useState('0');
  const [todoList, setTodoList] = useState([]);
  const [mode, setMode] = useState('date');
  const [show, setShow] = useState(false);
  const [createTaskModalIsVisible, setCreateTaskModalVisibility] = useState(false); 
  const [editTaskModalIsVisible, setEditTaskModalVisibility] = useState(false); 

  const createTable = async () => {
    console.warn("Test");
     await db.transaction(txn => {
      txn.executeSql('DROP TABLE IF EXISTS todos',[],
      (sqlTxn, res) => {
        console.log("Tabelle gelöscht");
      },
      error => {
        console.warn("Fehler beim löschen")
      },
      );
      txn.executeSql(
      'CREATE TABLE IF NOT EXISTS todos (id INTEGER PRIMARY KEY AUTOINCREMENT, todo_name VARCHAR(255), todo_note VARCHAR(255), todo_date TEXT, todo_time TEXT, todo_done INTEGER)',
      //'CREATE TABLE IF NOT EXISTS todos (id INTEGER PRIMARY KEY AUTOINCREMENT, todo_name VARCHAR(255), todo_note VARCHAR(255), todo_date TEXT, todo_time TEXT)',
      [],
      (sqlTxn, res) => {
        console.log("Tabelle erfolgreich erstellt.")
      },
      error => {
        console.warn("Fehler beim Erstellen der Tabelle "+error.message)
      },
      );
    
      // Fill the app with some tasks
      let task = createRandomTasks(500);
      for (let index = 0; index < task.length; index++) {
        const element = task[index];
        txn.executeSql(
          'INSERT INTO todos (todo_name, todo_note, todo_date, todo_time,todo_done) VALUES (?,?,?,?,?)',
        [element.name,element.note,element.date,element.time,element.done],
        (sqlTxn, res) => {
          console.log("Aufgabe "+index);
        },
        error => {
          console.warn("Fehler beim Hinzufügen von " +index+ " " +error.message)
        },
        )};

        getTodos()
    });
  }
      
      
     

  function createRandomTasks(amount) {
    
    let task = [];
    for (let index = 1; index <= amount; index++) {
      let element = {}
      element.name = "Aufgabe " + index;
      element.date = moment().format("YYYY-MM-DD");
      element.time = moment().format("HH:mm");
      element.done = 0;
      element.note = "Notiz Aufgabe " + index;
      task.push(element);
    }
    return task;
  }

  const addTodo =  async () => {
    if(!t_Name){
      alert('Bitte geben Sie einen Aufgaben-Namen ein.');
      return false;
    } 
    

     await db.transaction(txn => {
      txn.executeSql(
        'INSERT INTO todos (todo_name, todo_note, todo_date, todo_time,todo_done) VALUES (?,?,?,?,?)',
        [t_Name,t_Note,t_Date,t_Time,0],
        (sqlTxn, res) => {
          console.log("Aufgabe mit Titel "+t_Name+" und Notiz "+ t_Note);
          getTodos();
          setCreateTaskModalVisibility(false);
          setTodoName("");
          setTodoId("");
          setTodoNote("");
          setTodoDone(0);
          setTodoDate(moment().format("YYYY-MM-DD"));
          setTodoTime(moment().format("HH:mm"))
        },
        error => {
          console.warn('Fehler beim Hinzufügen von '+todo + " - " + error.message);
        }
      )
    });
  };

  const getTodos = async () => {
     await db.transaction(txn => {
      txn.executeSql(
        'SELECT * FROM todos ORDER BY todo_done DESC',
        [],
        (sqlTxn, res) => {
          console.log('Daten erfolgreich abgerufen.');
          let len = res.rows.length;
          if(len > 0){
            let results = [];
            for(let i = 0; i < len; i++){
              let item = res.rows.item(i);
              results.push({id: item.id, name: item.todo_name, note: item.todo_note, date: item.todo_date, time: item.todo_time,done: item.todo_done});
            }
            setTodoList(results);
          }
        },
        error => {
          console.warn('Fehler beim Abrufen der Daten' + " - " + error.message);
        }
      )
    });
  };

  const getTodoById = async (id) =>{
    await db.transaction(txn =>{
      txn.executeSql(
        'SELECT * FROM todos WHERE id=?',
      [id],
      (sqlTxn, res) => {
        let len= res.rows.length;
        if(len >1){
          console.warn('Fehler - mehrere Einträge mit der selben ID gefunden.')
          return;
        }
        setTodoName(res.rows.item(0).todo_name);
        setTodoDate(res.rows.item(0).todo_date);
        setTodoTime(res.rows.item(0).todo_time);
        setTodoNote(res.rows.item(0).todo_note);
        setTodoId(res.rows.item(0).id);
        console.log("Datensatz gefunden: "+res.rows.item(0).todo_name);
        setEditTaskModalVisibility(true);
      },
      error => {
        console.warn('Fehler beim Abrufen der Daten mit der id '+id+" - "+error.message)
      }
      )
    })
  };

  const hideEditAndClear = () => {
    setEditTaskModalVisibility(false);
    setTodoName("");
    setTodoId("");
    setTodoNote("");
    setTodoDone(0);
    setTodoDate(moment().format("DD.MM.YYYY"));
    setTodoTime(moment().format("HH:MM"))
  };

  

  const deleteTodoById = async (id) => { 
    
    await db.transaction(txn => {
      txn.executeSql(
        'DELETE FROM todos WHERE id=?',
        [t_id],
        (sqlTxn, res) => {
          console.log("Aufgabe mit id "+id+" wurde gelöscht");
          getTodos();
          setEditTaskModalVisibility(false);
          setTodoName("");
          setTodoList([]);
          setTodoNote("");
          setTodoDone(0);
          setTodoDate(moment().format("DD.MM.YYYY"));
          setTodoTime(moment().format("HH:MM"))
          setTodoId("");
        },
        error => {
          console.warn('Fehler beim Hinzufügen von '+todo + " - " + error.message);
        }
      )
    });
  };

  const updateTodoById = async (id) => {
    await db.transaction(txn => {
      txn.executeSql(
        "UPDATE todos SET todo_name=?, todo_date=?,todo_time=?,todo_note=? WHERE id=?",
        [t_Name,t_Date,t_Time,t_Note,t_id],
        (sqlTxn,res) => {
          console.log("Erfolgreich upgedatet!");
          getTodos();
          
          setTodoId("");
          setTodoName("");
          setTodoDate(moment().format("DD.MM.YYYY"));
          setTodoTime(moment().format("HH:MM"))
          setTodoNote("");
          setTodoDone(0);

          setTodoList([]);
          setEditTaskModalVisibility(false);
        },
        error => {
          console.warn("Fehler beim Updaten " + error.message);
        }
      )
    });
  };
  
  function ShowTodo1(done,id){
    let isDone = done;
    let todoId = id;
    if(isDone = 1){
      return (
        <TouchableOpacity
              onPress={() => console.log(todoId)/*setTodoToDoneById(item.id)*/}>
              <Image
                style={styles.tinyLogo}
                source={require('./src/assets/close.png')} />
            </TouchableOpacity>
      );
    } else {
      return(
      <TouchableOpacity
              onPress={() => updateTodoDoneById(todoId)}>
              <Image
                style={styles.tinyLogo}
                source={require('./src/assets/done.png')} />
            </TouchableOpacity>
      );
    }
  }
  
  const updateTodoDoneById = async(id,done) => {
    if(done == 1){
      done =0;
    } else {
      done = 1;
    }
    await db.transaction( txn => {
      txn.executeSql(
        'UPDATE todos SET todo_done=? WHERE id=?',
        [done,id],
        (sqlTxn,res) => {
          console.log("Update Done");
          getTodos();
        },
        error => {
          console.warn("Fehler beim Done updaten - " + error.message);
        }
      );
    });
  };

  const onChange = (event , selectedDate) => {
    const currentDate = selectedDate || t_Date;
    setShow(Platform.OS === 'ios');
    if(mode == 'date'){
      //console.warn(currentDate.split("-"));
      setTodoDate(moment(currentDate).format('YYYY-MM-DD'));
    }
    if(mode == 'time'){
      setTodoTime(moment(currentDate).format('HH:mm'));
    }
    
  };


  const renderTodo = ({item}) => {
    return(
      <View style={styles.itemView}>
        <View style={styles.itemContainerView}>
        <View style={styles.itemIconView}>
            {item.done == 1 && 
            <TouchableOpacity
              onPress={() => updateTodoDoneById(item.id,item.done)}>
              <Image
                style={styles.tinyLogo}
                source={require('./src/assets/close.png')} />
            </TouchableOpacity>}
            {item.done == 0 && 
            <TouchableOpacity
              onPress={() => updateTodoDoneById(item.id,item.done)}>
              <Image
                style={styles.tinyLogo}
                source={require('./src/assets/done.png')} />
            </TouchableOpacity>}
          </View>
          <View style={styles.itemTextView}>
            {item.done == 0 && <Text style={styles.itemName}>{item.name}</Text>}
            {item.done == 1 && <Text style={styles.itemNameDone}>{item.name}</Text>}
          </View>
          <View style={styles.itemIconView}>
            {item.done == 0 &&
            <TouchableOpacity
                  onPress={() => getTodoById(item.id)}>
              <Image
                style={styles.tinyLogo}
                source={require('./src/assets/edit.png')} />
            </TouchableOpacity>}
          </View>
        
        </View>
      </View>
    );
  };

  const showAddTask = () => {
    setCreateTaskModalVisibility(true);
    setTodoTime(t_Time => moment().format("HH:mm"));
    setTodoDate(t_Date => moment().format("YYYY-MM-DD"));
  }

  const showMode = (currentMode) => {
    setShow(true);
    setMode(currentMode);
  }


  const showTimePickerButton = () => {
    showMode('time');
  }

  const showDatePickerButton = () => {
    showMode('date');
  }

  useEffect( () => {
    //createTable();
    getTodos();
    }, []
  );

  return (
    <>
  <StatusBar barStyle='dark-content'/>
    <SafeAreaView style={styles.wrapper}>
      <View style={styles.headerView}>
        <Text style={styles.headerText}> Aufgabenliste </Text>
      </View>
      <View style={styles.listView}>
      {//<Button title="GetTasks" onPress={createTable}/>  
      }
      



        <FlatList 
          data={todoList} 
          renderItem={renderTodo}
          //key={todo => todo.id.toString}
          keyExtractor={ (item, index) => index.toString() }
        />
      </View>
      <View style={styles.createTaskView}>
        <TouchableOpacity
          style={styles.button}
          onPress={() => {
          //setCreateTaskModalVisibility(true);
          showAddTask();
        }}>
            <Text style={styles.buttonText}>Neue Aufgabe erstellen</Text>
        </TouchableOpacity>
      </View>
      
      <Modal
        animationType = {"slide"}
        transparent={true}
        visible={createTaskModalIsVisible}
      >
        <SafeAreaView style={styles.createTaskModalContainer}>
          <View style={styles.modalTitleContainer}>
            <View style={styles.modalTitleView}>
              <Text style={styles.modalTitleText}> 
                Neue Aufgabe erstellen 
              </Text>
            </View>
            
            <View style={styles.modalTitleCloseView}>
              <TouchableOpacity
              onPress={() => setCreateTaskModalVisibility(false)}>
                <Image
                  style={styles.tinyLogo}
                  source={require('./src/assets/close.png')} />
              </TouchableOpacity>
            </View>
          </View>
        <View style={styles.modalInfoContainerView}>
          <View style={styles.modalInfoTitleTextView}>
            <Text style={styles.modalInfoTitleText}>
              Titel:
            </Text>
          </View>
          <View style={styles.modalInfoTitleInputView}>
            <TextInput  
              placeholder="Titel der Aufgabe"
              value={t_Name}
              onChangeText={setTodoName}
              style={styles.modalInfoTitleInput}/>
          </View>
          
        </View>
          

        <View style={styles.modalInfoContainerView}>
            <View style={styles.modalInfoTitleTextView}>
              <Text style={styles.modalInfoTitleText}>
              Datum:
              </Text>
            </View>
          <View style={styles.modalInfoTitleInputView}>
          <TouchableOpacity
              onPress={showDatePickerButton}>
              <Image
                style={styles.tinyLogo}
                source={require('./src/assets/calendar.png')} />
            </TouchableOpacity>
            <Text>
              {t_Date}
            </Text>
          {show &&<DateTimePicker
        value={new Date()}
        display="compact"
        mode={mode}
        is24Hour={true}
        display='default'
        onChange={onChange}
      />}
          </View>
        </View>

        <View style={styles.modalInfoContainerView}>
            <View style={styles.modalInfoTitleTextView}>
              <Text style={styles.modalInfoTitleText}>
            Uhrzeit:
              </Text>
            </View>
          <View style={styles.modalInfoTitleInputView}>
          <TouchableOpacity
              onPress={showTimePickerButton}>
              <Image
                style={styles.tinyLogo}
                source={require('./src/assets/clock.png')} />
            </TouchableOpacity>
            <Text>
            {t_Time}
            </Text>
          </View>
        </View>

        <View style={styles.modalInfoContainerView}>
            <View style={styles.modalInfoTitleTextView}>
              <Text style={styles.modalInfoTitleText}>
              Notiz:
              </Text>
            </View>
          <View style={styles.modalInfoTitleInputView}>
        <TextInput  
          placeholder="Notiz"
          value={t_Note}
          onChangeText={setTodoNote}
          style={styles.modalInfoTitleInput}/>  
          </View>
        </View>
        <View style={styles.modalSubmitButtonView}>
          
          



        </View>
      </SafeAreaView>
    </Modal>
    
    <Modal
        animationType = {"slide"}
        transparent={true}
        visible={editTaskModalIsVisible}
      >
        <SafeAreaView style={styles.editTaskModalContainer}>
          <View style={styles.modalTitleContainer}>
            <View style={styles.modalTitleView}>
              <Text style={styles.modalTitleText}> Aufgabe bearbeiten </Text>
            </View>
            
            <View style={styles.modalTitleCloseView}>
              <TouchableOpacity
              onPress={(t_id) => updateTodoById(t_id)}>
                <Image
                  style={styles.tinyLogo}
                  source={require('./src/assets/save.png')} />
              </TouchableOpacity>
            </View>
            <View style={styles.modalTitleCloseView}>
              <TouchableOpacity
              onPress={(t_id) => 
                {
                  Alert.alert(
                    "Aufgabe löschen?",
                    "Wollen Sie die Aufgabe wirklich löschen?",
                    [
                      {
                        text: "Nein",
                        onPress: () => {
                          return;
                        }
                      },
                      {
                        text: "Ja",
                        onPress: (t_id) => {
                          deleteTodoById(t_id);}
                      }
                    ]
                  );
                }
              }
              >

                <Image
                  style={styles.tinyLogo}
                  source={require('./src/assets/delete.png')} />
              </TouchableOpacity>
            </View>
            <View style={styles.modalTitleCloseView}>
              <TouchableOpacity
              onPress={() => hideEditAndClear()}>
                <Image
                  style={styles.tinyLogo}
                  source={require('./src/assets/close.png')} />
              </TouchableOpacity>
            </View>
          </View>
        <View style={styles.modalInfoContainerView}>
          <View style={styles.modalInfoTitleTextView}>
            <Text style={styles.modalInfoTitleText}>
              Titel:
            </Text>
          </View>
          <View style={styles.modalInfoTitleInputView}>
            <TextInput  
              placeholder="Titel der Aufgabe"
              value={t_Name}
              onChangeText={setTodoName}
              style={styles.modalInfoTitleInput}/>
          </View>
          
        </View>
          

        <View style={styles.modalInfoContainerView}>
            <View style={styles.modalInfoTitleTextView}>
              <Text style={styles.modalInfoTitleText}>
              Datum:
              </Text>
            </View>
          <View style={styles.modalInfoTitleInputView}>
          <TouchableOpacity
              onPress={showDatePickerButton}>
              <Image
                style={styles.tinyLogo}
                source={require('./src/assets/calendar.png')} />
            </TouchableOpacity>
            <Text>
              {t_Date}
            </Text>
          {show &&<DateTimePicker
        value={new Date()}
        mode={mode}
        is24Hour={true}
        display='default'
        onChange={onChange}
      />}
          </View>
        </View>

        <View style={styles.modalInfoContainerView}>
            <View style={styles.modalInfoTitleTextView}>
              <Text style={styles.modalInfoTitleText}>
            Uhrzeit:
              </Text>
            </View>
          <View style={styles.modalInfoTitleInputView}>
          <TouchableOpacity
              onPress={showTimePickerButton}>
              <Image
                style={styles.tinyLogo}
                source={require('./src/assets/clock.png')} />
            </TouchableOpacity>
            <Text>
              {t_Time}
            </Text>
          </View>
        </View>

        <View style={styles.modalInfoContainerView}>
            <View style={styles.modalInfoTitleTextView}>
              <Text style={styles.modalInfoTitleText}>
              Notiz:
              </Text>
            </View>
          <View style={styles.modalInfoTitleInputView}>
        <TextInput  
          placeholder="Notiz"
          value={t_Note}
          onChangeText={setTodoNote}
          style={styles.modalInfoTitleInput}/>  
          </View>
        </View>
      </SafeAreaView>
    </Modal>

    


    </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: '#fff',
  },

  // Header 
  headerView: {
    flex: 1,
    maxHeight: 50,
    backgroundColor: 'skyblue',
    justifyContent: 'center',
    alignItems: 'center',
  },

  headerText: {
    fontSize: 20,
    fontWeight: 'bold',
  },

  // Liste
  listView:{
    flex: 10,
    margin: 10,
    borderRadius: 10,
    borderWidth: 2,
    borderColor: '#ddd',
  },
  itemView: {
    flex: 1,
    
    paddingVertical: 10,
    paddingHorizontal: 8,
    borderBottomWidth: 1,
    borderColor: '#ddd',
  },

  itemContainerView:{
    flexDirection: 'row',
    flex: 1,
  },

  itemIconView:{
    flex:1,
    padding: 5,
  },
  itemTextView:{
    flex: 9,
    padding: 5,
  },
  itemName:{
    fontSize: 18,
  },
  itemNameDone:{
    fontSize:18,
    textDecorationLine: 'line-through',
    color:'#999'
  },

  // Create Task Button
  createTaskView:{
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    maxHeight: 50,
    //backgroundColor: 'skyblue',
  },

  button: {
    display: 'flex',
    height: 40,
    borderRadius: 6,
    justifyContent: 'center',
    alignItems: 'center',
    width: '50%',
    backgroundColor: 'skyblue',
    shadowColor: 'skyblue',
    shadowOpacity: 0.5,
    shadowOffset: { 
      height: 10, 
      width: 0 
    },
    shadowRadius: 25,
  },  

  buttonText: {
    fontWeight: 'bold',
  },

  // Modal
  editTaskModalContainer:{
    height: '70%',
    flex:0, 
    backgroundColor: 'white',
    marginTop: 100,
    marginHorizontal: 10,
    borderRadius: 10,

    shadowColor: 'black',
    shadowOpacity: 0.75,
    shadowOffset: { 
      height: 10, 
      width: 0 
    },
    elevation: 5,
    shadowRadius: 25,
    },
  createTaskModalContainer: {
    height: '70%',
    flex:0, 
    marginTop: 100,
    marginHorizontal: 10,
    borderRadius: 10,
    backgroundColor: 'white',

    shadowColor: 'black',
    shadowOpacity: 0.75,
    shadowOffset: { 
      height: 10, 
      width: 0 
    },
    elevation: 5,
    shadowRadius: 25,
  },

  modalTitleContainer: {
    flex: 1, 
    maxHeight: 70,
    flexDirection: 'row',
    padding: 15,
    borderBottomColor: '#ddd',
    borderBottomWidth: 1,
    justifyContent:'space-between',
    alignItems: 'center'
    //backgroundColor: 'lightblue'
  },
  modalTitleView:{
    flex: 11,
  },

  modalTitleCloseView:{
    flex:1,
  },

  modalTitleText:{
      fontSize: 18,
      fontWeight: 'bold',
  },

  modalInfoContainerView: {
    padding: 15,
    marginTop: 10,
    marginHorizontal: 20,
    flexDirection: 'row',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#aaa'
  },

  modalInfoTitleTextView:{
    flex: 2,
  },
  modalInfoTitleText: {
    fontSize: 18,
    fontWeight: '600',
  },

  modalInfoTitleInputView: {
    flex: 4,
    borderBottomColor: '#000',
    borderBottomWidth: 1,
    paddingHorizontal: 5,
  },

  modalInfoTitleInput:{
    fontSize: 18,
    //textAlign: 'center',
  },

  modalSubmitButtonView:{
    flex: 1,
    marginTop: 20,
    borderTopColor: '#ddd',
    borderTopWidth: 1,
    justifyContent: 'center',
  }

});

export default App;